package com.example.demo.api;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("v1/")
public class EventRestController {

    @GetMapping("normal")
    public List<Map<String,String>> normal() {

        return getMaps();
    }

    @GetMapping("special")
    public List<Map<String,String>> special() {
        return getMaps();
    }

    private List<Map<String, String>> getMaps() {
        List<Map<String, String>> allData = new ArrayList<>();
        for (int i=1; i<10; i++) {
            Map<String, String> data = new HashMap<>();
            data.put("header", "Header" + i);
            data.put("title", "Title" + i);
            data.put("body", "Lorem ipsum dolor sit amet consectetur adipisicing elit. " +
                    "Earum quasi quaerat maiores laudantium assumenda nihil aliquid natus ea accusantium, " +
                    "eligendi et deserunt eos magnam unde sapiente minus autem vero eum.");
            data.put("footer", "Footer" + i);
            allData.add(data);
        }
        return allData;
    }

}
