package com.example.demo.api;

import com.auth0.jwt.JWT;
import com.example.demo.model.User;
import com.example.demo.model.view.UserView;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static com.example.demo.jwtsecurity.SecurityConstants.*;
import static com.example.demo.jwtsecurity.SecurityConstants.TOKEN_PREFIX;

@RestController
@RequestMapping("v1/users")
public class UserRestController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<User> getList() {
        return userService.list();
    }

    @PostMapping
    public Map<String, String> register(@RequestBody User _user, HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String orginalPassword = _user.getPassword();
        Map<String, String> response = new HashMap<>();
        _user.setPassword(encoder.encode(_user.getPassword()));
        User user = userService.register(_user);
        if (user != null) {
            String token = JWT.create()
                    .withSubject(user.getEmail())
                    .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                    .sign(HMAC512(SECRET.getBytes()));
            res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
            response.put("token", token);
            return response;
        }
        return null;
    }
}
