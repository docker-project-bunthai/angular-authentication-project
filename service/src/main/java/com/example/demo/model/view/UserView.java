package com.example.demo.model.view;


import lombok.Data;

@Data
public class UserView {
    private int id;
    private  String email;
}
