import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input()
  header = '';
  @Input()
  title = '';
  @Input()
  body = '';
  @Input()
  footer = '';


  @Input()
  type = 'event';
  typeClass = 'primary';

  constructor() {

  }

  ngOnInit() {
    if (this.type === 'special') {
      this.typeClass = 'success';
    }
  }

}
