import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  events: any;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.normal().subscribe(data => {
      this.events = data;
    }, err => {
      console.log(err);
    });
  }

}
