import { AuthGuard } from './auth.guard';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { EventComponent } from './event/event.component';
import { SpecialComponent } from './special/special.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthService } from './auth.service';
import { RegisterComponent } from './register/register.component';
import { TokenInterceptorServiceService } from './token-interceptor-service.service';

@NgModule({
  declarations: [
    AppComponent,
    EventComponent,
    SpecialComponent,
    LoginComponent,
    PageNotFoundComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  exports: [],
  providers: [AuthService, AuthGuard,
  //   {
  //   provide: HTTP_INTERCEPTORS,
  //   useClass: TokenInterceptorServiceService,
  //   multi: true
  // }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
