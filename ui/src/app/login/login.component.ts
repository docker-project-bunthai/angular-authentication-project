import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router) { }

  loginForm = this.fb.group({
    email: ['a@a.com'],
    password: ['123'],
  });

  ngOnInit() {
  }

  onLogin() {
    this.auth.login(this.loginForm.value).subscribe(data => {
      localStorage.setItem('token', data.token);
      this.router.navigate(['/special']);
    }, err => {
      console.log(err);
    });
  }

}
