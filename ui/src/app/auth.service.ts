import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router) { }

  url = 'http://localhost:8080';

  register(user: any) {
    return this.http.post<any>(this.url + '/v1/users', user);
  }

  login(user: any) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post<any>(this.url + '/login', user);
  }

  normal() {
    return this.http.get<any>(this.url + '/v1/normal');
  }

  special() {
    return this.http.get<any>(this.url + '/v1/special', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    });
    // return this.http.get<any>(this.url + '/v1/special');
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/event']);
  }
  isLogin() {
    return !!localStorage.getItem('token');
  }
}
