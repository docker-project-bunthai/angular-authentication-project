import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-special',
  templateUrl: './special.component.html',
  styleUrls: ['./special.component.scss']
})
export class SpecialComponent implements OnInit {

  specials: any;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.authService.special().subscribe(data => {
      this.specials = data;
    }, err => {
      console.log(err);
      this.router.navigate(['/login']);
    });
  }
}
