import { AuthService } from './../auth.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) { }

  registerForm = this.fb.group({
    email: [''],
    password: [''],
    passwordConfirm: ['']
  });

  register() {
    this.authService.register(this.registerForm.value).subscribe(
      data => {
        localStorage.setItem('token', data.token);
        this.router.navigate(['/special']);
      }, err => {
        console.log(err);
      });
  }

  ngOnInit() {
  }

}
